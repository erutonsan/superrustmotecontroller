fn main() {
    // use std::io;
    use std::net::UdpSocket;
    use gilrs::{Gilrs, Event, EventType, Button};
    let mut gilrs = Gilrs::new().unwrap();

    for (id, gamepad) in gilrs.gamepads() {
        println!("{}: {} is {:?}", id, gamepad.name(), gamepad.power_info());
    }

    /*
    // TODO: Ask user to select a gamepad or keyboard
    println!("Please select an input mode ('k' for keyboard):");

    let mut input = String::new();

    io::stdin()
        .read_line(&mut input)
        .expect("Failed to read input");
    */

    let socket = UdpSocket::bind("0.0.0.0:0")
        .expect("Couldn't bind to address");
    socket.connect("127.0.0.1:5005")
        .expect("Couldn't connect to server");

    loop {
        while let Some(Event { id, event, time }) = gilrs.next_event() {
            let mut action = String::new();
            match event {
                EventType::ButtonPressed(btn, _) => {
                    let btn_str = match btn {
                        Button::DPadUp => "HAT_UP",
                        Button::DPadDown => "HAT_DOWN",
                        Button::DPadLeft => "HAT_LEFT",
                        Button::DPadRight => "HAT_RIGHT",
                        Button::South => "BUTTON_A",
                        Button::East => "BUTTON_B",
                        Button::West => "BUTTON_Y",
                        Button::North => "BUTTON_X",
                        Button::RightTrigger => "SHOULDER_RIGHT",
                        Button::LeftTrigger => "SHOULDER_LEFT",
                        Button::Start => "BUTTON_START",
                        Button::Select => "BUTTON_SELECT",
                        _ => "",
                    };
                    if !btn_str.is_empty() {
                        action = format!("{}_DOWN", btn_str);
                    }
                },
                EventType::ButtonReleased(btn, _) => {
                    let btn_str = match btn {
                        Button::DPadUp => "HAT_UP",
                        Button::DPadDown => "HAT_DOWN",
                        Button::DPadLeft => "HAT_LEFT",
                        Button::DPadRight => "HAT_RIGHT",
                        Button::South => "BUTTON_A",
                        Button::East => "BUTTON_B",
                        Button::West => "BUTTON_Y",
                        Button::North => "BUTTON_X",
                        Button::RightTrigger => "SHOULDER_RIGHT",
                        Button::LeftTrigger => "SHOULDER_LEFT",
                        Button::Start => "BUTTON_START",
                        Button::Select => "BUTTON_SELECT",
                        _ => "",
                    };
                    if !btn_str.is_empty() {
                        action = format!("{}_UP", btn_str);
                    }
                },
                _ => (),
            };
            if !action.is_empty() {
                println!("Sending: {}", action);
                socket.send(action.as_bytes())
                    .expect("Couldn't send keypress");
            }
        }
    }
}
